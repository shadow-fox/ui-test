# Margareth Mañunga UI Test

[**Staging for HMTL and CSS**](https://ui-test-margareth-layout-and-css-only.netlify.app/components/preview/home.html)

[**Staging for HMTL CSS and JS**](https://margareth-ui-test.netlify.app/components/preview/home.html)

**Project Overview:** The project is set up using Webpack as a module bundler, Fractal to assemble and preview modular components using handlebars, sass, typescript and vue, for a final build on web supported and legible formats.

## Installation 
 	-  npm install
 	-  npm run fractal (for fractal serving, watching and build)
 	-  npm run build (for building/minifying css/js only)


## Layout

- **HTML5 semantics and CSS3 :** Layout is built using HTML5 semantics where it made sense (header, main, section, footer). CSS3 features like *box-shadow, opacity, RGBA, * and many other features like transition for hover/focus states. I also used **BEM** naming convention, keeping the nesting to reasonable levels.
- **Responsive Design:** Responsive design using different media queries and breakpoints with a *mobile-first* approach
- **CSS preprocessing with SASS:** use of **sass mixing, variables and functions** to make attributes scalable and easy to modify. Also taking advantage of sass nesting properties along with BEM naming conventions where it made sense (I find that sometimes *over-nesting* can make the code harder to read). 
- **Fractal:** modular components built using Fractal, which allows for data feeding using JSON and handlebars.
- **Extra Features:** 
	- Mobile menu  noJS support
	- Banner dismissal when clicking on the X button.
	- hover and focus styles for some elements not included on the flat. (Still missing some usual expected web behaviors and events not mapped out on the test requirements, but I tried to keep it close to what a real client project would require)
	- Wave validation : passed (some contrast warnings present but these might be false positives from design)
	- HTML validation : no errors 


## Interaction - JS

Functional interaction on the 4 voting cards feed. Built using VueJs, Javascript and localStorage to save the vote count.

- **JSON Data:** checks for the existing localStorage object as a string and defaults to a base array of objects if empty. Then passed as a prop to the VueJS component to display cards.
- **Voting:** Voting system works as required, built using VueJS v-model as a main tool, and different methods and bindings to toggle states and push data to the localStorage object. VueJS transitions also used.
-  **Extra Features and Notes:** 
	- *Vote Now* button is disabled until the user selects a vote, to avoid submitting a null vote.
	-  Thumb icon next to the name changes according to the max percentage (teal and up when up votes are a larger percentage and yellow when down votes are)
	- Fade in and out animations
	- If the bars reach a point where their percentage width is smaller than the content, as a fallback I added a min width to keep the text from overflowing)


## Deployment 

Building+Deployment via Netlify and Bitbucket


## Notes
 	- No git flow used since I didn't think it made sense for the test requirements.
 	- I didn't have enough time for unit testing, but normally I would use Jest for JS/TS with Vue Test Utils for Vue components. 
 	In this case I would only test the Vue component which has the most diverse scenarios for user usage.
