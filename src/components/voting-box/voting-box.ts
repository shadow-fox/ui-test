import votingFeedComponent from "./voting-feed.vue";
import Vue from "vue";
class votingFeed {
  protected element: HTMLElement;
  basePeopleObject: {};
  peopleObject: {};
  peopleObjectStored: '';
  public vueApp: Vue;

  constructor(element: HTMLElement) {
    this.element = element;
    this.peopleObjectStored = JSON.parse(window.localStorage.getItem('people'));
    this.basePeopleObject = [
        {
            "name": "Kanye West",
            "img": "/img/kanye.jpg",
            "date": "1 month",
            "source": "Entertainment",
            "description": "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
            "upvotes": 12,
            "downvotes": 8
        },
        {
            "name": "Mark Zuckerberg",
            "img": "/img/mark.jpg",
            "date": "1 month",
            "source": "Business",
            "description": "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
            "upvotes": 3,
            "downvotes": 5
        },
        {
            "name": "Cristina Fernández de Kirchner",
            "img": "/img/cristina.jpg",
            "date": "1 month",
            "source": "Politics",
            "description": "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
            "upvotes": 11,
            "downvotes": 12
        },
        {
            "name": "Malala Yousafzai",
            "img": "/img/malala.jpg",
            "date": "1 month",
            "source": "Entertainment",
            "description": "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
            "upvotes": 10,
            "downvotes": 1
        }
    ];
    let vueElement = this.element;
    this.checkForLocalObject();
    this.vueApp = new Vue({
        components: {
          "voting-feed": votingFeedComponent
        },
        el: vueElement,
        data: {
          people: this.peopleObject,
        }
    });
  }
  checkForLocalObject() {
    if (this.peopleObjectStored) {
      this.peopleObject = this.peopleObjectStored;
    } else {
      this.peopleObject = this.basePeopleObject;
    }
  }
}

function votingFeedInit() {
  let feedContainer = document.querySelector(".voting-box-feed-container") as HTMLElement;
  if (feedContainer) {
    new votingFeed(feedContainer);
  }
}
export { votingFeed, votingFeedInit };