
import * as _debounce from "lodash.debounce";
class bannerDismiss {
    element: HTMLElement;
    body:HTMLElement;
    bannerClose : HTMLElement;
    navDrawer : HTMLElement;
    bannerHeight;
    banner;
  
    constructor(element) {
      this.element = element;
      this.bannerClose = this.element.querySelector(".banner__close-icon");
      this.banner = this.element.querySelector(".banner");
      this.closeButtonListener();
      this.bannerHeightListener()
      
    }
    bannerHeightListener() {
      this.bannerHeight = this.banner.clientHeight;
      this.element.style.height = this.bannerHeight + 48 + "px";
    }
    closeButtonListener() {
      if(this.bannerClose) {
        this.bannerClose.addEventListener("click", event => {
          event.preventDefault();
          this.element.style.height = "0px";
          this.element.style.opacity = "0";
          setTimeout(() => {
            this.element.classList.remove("banner-wrapper--show");
          }, 400);
        });
      }
      window.addEventListener("resize",_debounce (e => {
          this.bannerHeightListener();
        }, 100)
      );
    }
  }
  
  function bannerDismissInit() {
    let bannerContainer = document.querySelector(".banner-wrapper");
    if (bannerContainer) {
      new bannerDismiss(bannerContainer);
    }
  }
  
  export { bannerDismiss, bannerDismissInit };
  