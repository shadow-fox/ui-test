
class mainHeader {
  element: HTMLElement;
  body:HTMLElement;
  navToggle : HTMLElement;
  navDrawer : HTMLElement;

  constructor(element) {
    this.element = element;
    this.navToggle = this.element.querySelector(".main-header__toggle");
    this.navDrawer = this.element.querySelector(".main-header__drawer");
    this.body = document.querySelector("body");
    this.navListener();
  }
  navListener() {
    if(this.navToggle && this.navDrawer) {
      this.navToggle.addEventListener("click", event => {
        event.preventDefault();
        if(!this.body.classList.contains("main-header--nav-open")){
          this.body.classList.add("main-header--nav-open");
          setTimeout(() => {
            this.navDrawer.classList.add("main-header__drawer--animate")
          }, 300);
        } else {
          this.navDrawer.classList.remove("main-header__drawer--animate");
          setTimeout(() => {
            this.body.classList.remove("main-header--nav-open")
          }, 300);
        }
      });
    }
  }
}

function mainHeaderInit() {
  let mainNavigation = document.querySelector(".main-header");
  if (mainNavigation) {
    new mainHeader(mainNavigation);
  }
}

export { mainHeader, mainHeaderInit };
