//Components
import { mainHeaderInit } from "../components/header/header";
import { votingFeedInit } from "../components/voting-box/voting-box";
import { bannerDismissInit } from "../components/banner/banner";

//Triggers
mainHeaderInit();
votingFeedInit();
bannerDismissInit();

let body = document.querySelector("body");
if (body) {
    body.classList.add("js");
}